import { createStore, applyMiddleware, compose } from 'redux'
import reducers from './reducers'
import thunk from "redux-thunk";



export default function configureStore(initialState = {}) {
    const composeEnhancers =
        typeof window === "object" &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            }) : compose;

    const enhancer = composeEnhancers(
        applyMiddleware(thunk),
    );
    const task = createStore(reducers, enhancer);


    task.asyncReducers = {};

    return task;
}