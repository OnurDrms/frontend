import axios from 'axios';

export const Tasks = (value) => ({
    type: "TASK",
    payload: value
});

export const getTasks = () => {

    return async (dispatch) => {
        await axios({
                    method: 'get',
                    url: 'http://localhost:8080/task/get'
                }).then(res => {
                    dispatch(Tasks(res.data.results));
                });
    };
};