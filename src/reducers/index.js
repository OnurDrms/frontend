import {combineReducers} from "redux";

const getTasks = (state=[], action)=>{
    if (action.type ==="TASK"){
        return action.payload;
    }
    return state;
};

export default combineReducers({
    tasks: getTasks
});
