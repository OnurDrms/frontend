import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './task';
import './App.css';

import Routes from './routes'


const initialState = {};
const task = configureStore(initialState);

ReactDOM.render(
    <Provider store={task}>
        <Routes />
    </Provider>
    , document.getElementById('root')
);