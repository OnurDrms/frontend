import React from 'react'
import { BrowserRouter as Router, Route } from "react-router-dom";

import AddTask from './pages/AddTask';
import TaskDetail from "./pages/TaskDetail";
import Homepage from './pages/HomePage';
const Routes = props => {
    return (
        <div>
            <Router>

                <div>
                    <Route path="/" exact component={Homepage}/>
                    <Route path="/add-task" exact component={AddTask} />
                    <Route path="/task-detail" exact component={TaskDetail} />
                </div>
            </Router>
        </div>
    )
};

export default Routes;
