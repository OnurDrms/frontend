import React from 'react';
import {getTasks} from '../action/index';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import Header from './Header'

class HomePage extends React.Component {
    componentDidMount(){
        this.props.getTasks();
    }
    renderTasksDisabled(){
        return this.props.tasks.map((element, index) => {
                if(element.isCompleted) {
                    return (
                            <div className="col-4" key={index}>
                                <div className="card text-center">
                                    <div className="card-body">
                                        <h5 className="card-title">{element.taskName}</h5>
                                        <p className="card-text">{element.taskTitle}</p>
                                    </div>
                                </div>
                            </div>
                    )
                }
            })
    }
    renderTasks(){
        return this.props.tasks.map((element, index) => {
            if(element.isCompleted !== true) {
                return (
                        <div className="col-4" key={index}>
                            <div className="card text-center">
                                <div className="card-body">
                                    <h5 className="card-title">{element.taskName}</h5>
                                    <p className="card-text">{element.taskTitle}</p>
                                    <Link to={{
                                        pathname: `/task-detail`,
                                        myCustomProps: {element}

                                    }} className="btn btn-primary">Show</Link>
                                </div>
                            </div>
                        </div>
                )
            }
        })
    }
    render() {
        return (
            <div>
                <Header/>
                <div className="row">
                    <div className="container">
                        <ul className="nav nav-tabs" id="myTab" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" id="completed-tab" data-toggle="tab" href="#completed" role="tab"
                                   aria-controls="home" aria-selected="true">Completed</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" id="incompleted-tab" data-toggle="tab" href="#incompleted" role="tab"
                                   aria-controls="profile" aria-selected="false">Incompleted</a>
                            </li>
                        </ul>
                        <div className="tab-content" id="myTabContent">
                            <div className="tab-pane fade show active" id="completed" role="tabpanel"
                                 aria-labelledby="completed-tab"><div className="row"> {this.renderTasksDisabled()}</div>
                            </div>
                            <div className="tab-pane fade" id="incompleted" role="tabpanel"
                                 aria-labelledby="incompleted-tab"><div className="row"> {this.renderTasks()}</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = ({tasks}) => {
    return {
        tasks,
    };
};
export default connect(mapStateToProps, {getTasks})(HomePage);