import React from 'react';
import { Link } from 'react-router-dom';



function Header() {
    return (
        <div>
            <header className="header">
                <div className="container">
                    <article className="header-top text-center">
                        <h1>To Do List</h1>
                    </article>
                    <nav className="menu">
                        <ul>
                            <li><Link to="/">Works</Link></li>
                            <li><Link to="/add-task">Add Task</Link></li>
                        </ul>
                    </nav>
                </div>
            </header>
        </div>
    );
}

export default Header;