import React from 'react';
import axios from 'axios';
import DateTime from 'react-datetime';
import moment from 'moment';
import Header from './Header';

export default class AddTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            taskName: '',
            taskTitle: '',
            taskSubTitle: '',
            taskDeadLine: new Date(),
            taskAssignedUser: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        let name = event.target.name;
        this.setState({[name]: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        axios.post('http://localhost:8080/add/task', {
            data: {
                taskName: this.state.taskName,
                taskTitle: this.state.taskTitle,
                taskSubTitle: this.state.taskSubTitle,
                taskDeadLine: this.state.taskDeadLine,
                taskAssignedUser: this.state.taskAssignedUser,
                isCompleted: false
            }
        }).then(obj => {
            console.log(obj.data);
        })
        this.setState({
            taskName: '',
            taskTitle: '',
            taskSubTitle: '',
            taskDeadLine: new Date(),
            taskAssignedUser: ''});
    }


    render() {
        return (
            <div>
                <Header/>
                <div className="row">
                    <div className="container">
                        <div className="col-3"></div>
                        <div className="col-6">
                            <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label style={{color: "#fff"}}>Name:</label>
                                        <input type="text" className="form-control" name="taskName" value={this.state.taskName} onChange={this.handleChange} placeholder="Enter Task Name"/>
                                    </div>
                                    <div className="form-group">
                                        <label style={{color: "#fff"}}>Deadline:</label>

                                        <DateTime name="newapp_time"
                                                  onChange={(e) => { this.setState({ taskDeadLine: moment(e).toJSON() }) }} />
                                    </div>
                                    <div className="form-group">
                                        <label style={{color: "#fff"}}>Title:</label>
                                        <input type="text" className="form-control" name="taskTitle" value={this.state.taskTitle} onChange={this.handleChange} placeholder="Enter Task Title"/>
                                    </div>
                                    <div className="form-group">
                                        <label style={{color: "#fff"}}>Sub Title:</label>
                                        <textarea type="text" className="form-control" name="taskSubTitle" value={this.state.taskSubTitle} onChange={this.handleChange} placeholder="Enter Task Sub Title"/>
                                    </div>
                                    <div className="form-group">
                                        <label style={{color: "#fff"}}>Assigned User:</label>
                                        <textarea type="text" className="form-control" name="taskAssignedUser" value={this.state.taskAssignedUser} onChange={this.handleChange} placeholder="Enter Assigned User"/>
                                    </div>
                                    <button type="submit" className="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        <div className="col-3"></div>
                    </div>
                </div>
            </div>
        );
    }
}
