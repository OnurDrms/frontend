import React from 'react';
import Header from './Header';
import moment from 'moment';
import axios from "axios";
class TaskDetail extends React.Component {

    componentDidMount() {

            var end = new Date(this.props.location.myCustomProps.element.taskDeadLine);

            var _second = 1000;
            var _minute = _second * 60;
            var _hour = _minute * 60;
            var _day = _hour * 24;
            var timer;

            function showRemaining() {
                var now = new Date();

                var distance = end - now;
                if (distance === 0) {
                    clearInterval(timer);
                    document.getElementById('countdown').innerHTML = 'Time is Over!';
                    return;
                }
                var days = Math.floor(distance / _day);
                var hours = Math.floor((distance % _day) / _hour);
                var minutes = Math.floor((distance % _hour) / _minute);
                var seconds = Math.floor((distance % _minute) / _second);
                if( document.getElementById("countdown")) {
                    document.getElementById("countdown").innerHTML = days + ' day ';
                    document.getElementById("countdown").innerHTML += hours + ' hour ';
                    document.getElementById("countdown").innerHTML += minutes + ' minute ';
                    document.getElementById("countdown").innerHTML += seconds + ' second';
                }
            }
            timer = setInterval(showRemaining, 1000);

        var $ = window.$;
        var name = this.props.location.myCustomProps.element.taskName;
        $("#exampleCheck1").change(function() {
            if($(this).prop('checked')) {
                axios.post('http://localhost:8080/edit/task', {
                    data: {
                        taskName: name,
                        isCompleted: true
                    }
                }).then(obj => {
                    console.log(obj.data);
                });
                window.location.href = "/";
            }

        });
    }

    render() {
        let value = this.props.location.myCustomProps;
        return (
            <div>
                <Header/>
                <div className="row">
                    <div className="container">
                        <div className="card text-center">
                            <div className="card-header">
                                {value.element.taskName}
                            </div>
                            <div className="card-body">
                                <h5 className="card-title">{value.element.taskTitle}</h5>
                                <p className="card-text">{value.element.taskSubTitle}</p>
                                <p className="card-deadline">{moment(value.element.taskDeadLine).format("DD/MM/YYYY")}</p>
                                <p className="card-assignedUser">{value.element.taskAssignedUser}</p>
                                <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                    <label className="form-check-label">Completed ? </label>
                                </div>
                                <div id="countdown"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
export default TaskDetail;